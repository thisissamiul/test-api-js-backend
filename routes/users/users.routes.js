const { Router } = require("express");
const actions = require("./users.actions");
const validator = require("./users.validator");

module.exports = Router()
  .post(
    "/users/register",
    ...validator.registerUser,
    actions.registerUser,
  )
  .post(
    "/users/login",
    ...validator.loginUser,
    actions.loginUser,
  );

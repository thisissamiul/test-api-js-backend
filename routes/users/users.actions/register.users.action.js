const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { CREATED } = require("../../../constants/http-codes");
const { Conflict } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");
const { hashPassword } = require("../../../services/password.service");
const JwtService = require("../../../services/jwt.service");
const { jwt: jwtConfig } = require("../../../config");

/**
 * POST /users/register
 * Создание пользователя
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<Object>}
 */
async function registerUser(req, res) {
  logger.init("create user");
  const data = matchedData(req);

  const userExists = await db.user.findUnique({ where: { email: data.email } });
  if (userExists) {
    throw new Conflict("User with this email already exists");
  }

  const hashedPassword = await hashPassword(data.password);
  const user = await db.user.create({
    data: {
      fullName: data.fullName,
      email: data.email,
      password: hashedPassword,
    },
    select: {
      id: true,
      fullName: true,
      email: true,
    },
  });

  const token = new JwtService(jwtConfig).encode({ id: user.id }).data;

  res.header("Authorization", `Bearer ${token}`);
  res.status(CREATED).json(user);
  logger.success();
}

module.exports = {
  registerUser,
};

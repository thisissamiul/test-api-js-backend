const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const JwtService = require("../../../services/jwt.service");
const { db } = require("../../../services/database.service");
const { comparePassword } = require("../../../services/password.service");
const { Unauthorized } = require("../../../constants/errors");
const { OK } = require("../../../constants/http-codes");
const jwtConfig = require("../../../config").jwt;

/**
 * POST /users/login
 * Служебный эндпоинт для получения токена авторизации пользователя.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function loginUser(req, res) {
  logger.init("get user auth");
  const data = matchedData(req);

  const user = await db.user.findUnique({
    where: {
      email: data.email,
    },
  });
  if (!user) throw new Unauthorized();

  const isPasswordValid = await comparePassword(data.password, user.password);
  if (!isPasswordValid) throw new Unauthorized();

  const token = new JwtService(jwtConfig).encode({ id: user.id }).data;

  res.header("Authorization", `Bearer ${token}`);
  logger.success();
  return res.status(OK).json({
    id: user.id,
    fullName: user.fullName,
    email: user.email,
  });
}

module.exports = {
  loginUser,
};

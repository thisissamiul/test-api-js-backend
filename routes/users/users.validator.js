const { body } = require("express-validator");
const { UnprocessableEntity } = require("../../constants/errors");
const validate = require("../../middleware/validation.middleware");

const loginUser = [
  body("email")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "email: parameter is required",
    })
    .bail()
    .isEmail()
    .withMessage({
      code: UnprocessableEntity,
      message: "email: parameter must be a valid email",
    }),
  body("password")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "password: parameter is required",
    }),
  validate,
];

const registerUser = [
  body("email")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "email: parameter is required",
    })
    .bail()
    .isEmail()
    .withMessage({
      code: UnprocessableEntity,
      message: "email: parameter must be a valid email",
    }),
  body("password")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "password: parameter is required",
    }),
  body("fullName")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "name: parameter is required",
    }),
  validate,
];

module.exports = { loginUser, registerUser };

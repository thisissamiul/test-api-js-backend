const { body, param } = require("express-validator");
const { UnprocessableEntity } = require("../../constants/errors");
const validate = require("../../middleware/validation.middleware");

// Только российские номера
const PHONE_REGEX = /^(?:\+7|8)(?:\d{10})$/;

const getOne = [
  param("id").isString().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  validate,
];

const editOne = [
  param("id").isString().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  body("firstname").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "firstname: parameter must be a string",
  }),
  body("lastname").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "lastname: parameter must be a string",
  }),
  body("patronymic").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "patronymic: parameter must be a string",
  }),
  body("phone")
    .optional()
    .custom((value) => {
      if (!PHONE_REGEX.test(value)) {
        throw new Error();
      }
      return true;
    })
    .withMessage({
      code: UnprocessableEntity,
      message: "phone: parameter must be a valid russian phone number",
    }),
  body("email").optional().isEmail().withMessage({
    code: UnprocessableEntity,
    message: "email: parameter must be a valid email",
  }),
  validate,
];

const deleteOne = [
  param("id").isString().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  validate,
];

const createOne = [
  body("firstname")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "firstname: parameter is required",
    })
    .bail()
    .isString()
    .withMessage({
      code: UnprocessableEntity,
      message: "firstname: parameter must be a string",
    }),
  body("lastname")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "lastname: parameter is required",
    })
    .bail()
    .isString()
    .withMessage({
      code: UnprocessableEntity,
      message: "lastname: parameter must be a string",
    }),
  body("patronymic").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "patronymic: parameter must be a string",
  }),
  body("phone")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "phone: parameter is required",
    })
    .bail()
    .custom((value) => {
      // Только российские номера
      if (!PHONE_REGEX.test(value)) {
        throw new Error();
      }
      return true;
    })
    .withMessage({
      code: UnprocessableEntity,
      message: "phone: parameter must be a valid russian phone number",
    }),
  body("email")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "email: parameter is required",
    })
    .bail()
    .isEmail()
    .withMessage({
      code: UnprocessableEntity,
      message: "email: parameter must be a valid email",
    }),
  validate,
];

module.exports = { getOne, editOne, createOne, deleteOne };

const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const { NotFound } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * GET /contacts/:id
 * Эндпоинт получения данных контакта.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function getOne(req, res) {
  logger.init("get contact");
  const data = matchedData(req);

  const contact = await db.contact.findUnique({ where: { id: data.id } });
  if (!contact) {
    throw new NotFound("Contact not found");
  }

  res.status(OK).json(contact);
  logger.success();
}

module.exports = {
  getOne,
};

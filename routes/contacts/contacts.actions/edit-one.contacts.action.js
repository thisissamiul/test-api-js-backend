const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const { NotFound, Conflict } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * PATCH /contacts/:id
 * Эндпоинт редактирования данных контакта.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function editOne(req, res) {
  logger.init("edit contact");
  const data = matchedData(req);

  const contact = await db.contact.findUnique({ where: { id: data.id } });
  if (!contact) {
    throw new NotFound("Contact not found");
  }

  const existingContact = await db.contact.findFirst({
    where: {
      OR: [{ phone: data.phone }, { email: data.email }],
      NOT: {
        id: data.id,
      },
    },
  });
  if (existingContact) {
    throw new Conflict("Contact with this phone or email already exists");
  }

  if (data?.phone?.startsWith("8")) {
    data.phone = data.phone.replace("8", "+7");
  }
  const updatedContact = await db.contact.update({
    where: { id: data.id },
    data,
  });

  res.status(OK).json(updatedContact);
  logger.success();
}

module.exports = {
  editOne,
};

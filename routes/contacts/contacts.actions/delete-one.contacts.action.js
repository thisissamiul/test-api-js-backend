const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { NO_CONTENT } = require("../../../constants/http-codes");
const { NotFound } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * DELETE /contacts/:id
 * Эндпоинт удаления контакта.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function deleteOne(req, res) {
  logger.init("delete contact");
  const data = matchedData(req);

  const contact = await db.contact.findUnique({ where: { id: data.id } });
  if (!contact) {
    throw new NotFound("Contact not found");
  }

  await db.contact.delete({ where: { id: data.id } });

  res.status(NO_CONTENT).send();
  logger.success();
}

module.exports = {
  deleteOne,
};

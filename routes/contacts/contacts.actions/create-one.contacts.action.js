const { matchedData } = require("express-validator");

const logger = require("../../../services/logger.service")(module);
const { CREATED } = require("../../../constants/http-codes");
const { Conflict } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * POST /contacts
 * Эндпоинт создания нового контакта.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function createOne(req, res) {
  logger.init("create contact");
  const data = matchedData(req);

  const existingContact = await db.contact.findFirst({
    where: { OR: [{ phone: data.phone }, { email: data.email }] },
  });
  if (existingContact)
    throw new Conflict("Contact with this phone or email already exists");

  if (data.phone.startsWith("8")) {
    data.phone = data.phone.replace("8", "+7");
  }
  const contact = await db.contact.create({ data });

  res.status(CREATED).json(contact);
  logger.success();
}

module.exports = {
  createOne,
};

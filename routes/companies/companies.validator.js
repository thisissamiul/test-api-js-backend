const { body, param, query } = require("express-validator");
const { UnprocessableEntity } = require("../../constants/errors");
const validate = require("../../middleware/validation.middleware");

const STATUSES = ["active", "inactive"];
const TYPES = ["agent", "contractor"];
const SORTING = ["asc", "desc"];

const createOne = [
  body("name")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "name: parameter is required",
    })
    .bail()
    .isString()
    .withMessage({
      code: UnprocessableEntity,
      message: "name: parameter has incorrect format",
    }),
  body("shortName")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "shortName: parameter is required",
    })
    .bail()
    .isString()
    .withMessage({
      code: UnprocessableEntity,
      message: "shortName: parameter has incorrect format",
    }),
  body("businessEntity").isIn(["ООО", "АО", "ПАО"]).withMessage({
    code: UnprocessableEntity,
    message: "businessEntity: parameter has invalid value",
  }),
  body("address")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "address: parameter is required",
    })
    .bail()
    .isString()
    .withMessage({
      code: UnprocessableEntity,
      message: "address: parameter has incorrect format",
    }),
  body("contractNo")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "contractNo: parameter is required",
    })
    .bail()
    .isString()
    .withMessage({
      code: UnprocessableEntity,
      message: "contractNo: parameter has incorrect format",
    }),
  body("contractIssueDate")
    .notEmpty()
    .withMessage({
      code: UnprocessableEntity,
      message: "contractIssueDate: parameter is required",
    })
    .bail()
    .isISO8601()
    .withMessage({
      code: UnprocessableEntity,
      message: "contractIssueDate: parameter has incorrect format",
    }),
  body("type")
    .isArray()
    .withMessage({
      code: UnprocessableEntity,
      message: "type: parameter has incorrect format",
    })
    .isIn(TYPES)
    .withMessage({
      code: UnprocessableEntity,
      message: "type: parameter has incorrect value",
    }),
  body("status").isIn(STATUSES).withMessage({
    code: UnprocessableEntity,
    message: "status: parameter has incorrect value",
  }),
  body("contactId").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "contactId: parameter has incorrect format",
  }),
  validate,
];

const getOne = [
  param("id").isString().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  validate,
];

const deleteOne = [
  param("id").isString().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  validate,
];

const editOne = [
  param("id").isString().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  body("name").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "name: parameter has incorrect format",
  }),
  body("shortName").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "shortName: parameter has incorrect format",
  }),
  body("businessEntity").optional().isIn(["ООО", "АО", "ПАО"]).withMessage({
    code: UnprocessableEntity,
    message: "businessEntity: parameter has invalid value",
  }),
  body("address").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "address: parameter has incorrect format",
  }),
  body("contractNo").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "contractNo: parameter has incorrect format",
  }),
  body("contractIssueDate").optional().isISO8601().withMessage({
    code: UnprocessableEntity,
    message: "contractIssueDate: parameter has incorrect format",
  }),
  body("type")
    .optional()
    .isArray()
    .withMessage({
      code: UnprocessableEntity,
      message: "type: parameter has incorrect format",
    })
    .isIn(TYPES)
    .withMessage({
      code: UnprocessableEntity,
      message: "type: parameter has incorrect value",
    }),
  body("status").optional().isIn(STATUSES).withMessage({
    code: UnprocessableEntity,
    message: "status: parameter has incorrect value",
  }),
  body("contactId").optional({ nullable: true }).isString().withMessage({
    code: UnprocessableEntity,
    message: "contactId: parameter has incorrect format",
  }),
  validate,
];

const getMany = [
  query("take").optional().isNumeric().withMessage({
    code: UnprocessableEntity,
    message: "take: parameter has incorrect format",
  }),
  query("startingAfter").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "startingAfter: parameter has incorrect format",
  }),
  query("endingBefore").optional().isString().withMessage({
    code: UnprocessableEntity,
    message: "endingBefore: parameter has incorrect format",
  }),
  query("filter.status").optional().isIn(STATUSES).withMessage({
    code: UnprocessableEntity,
    message: "filter.status: parameter has incorrect value",
  }),
  query("filter.type")
    .optional()
    .custom((value) => {
      try {
        const parsed = JSON.parse(value);
        if (!Array.isArray(parsed)) {
          throw new Error();
        }
        return true;
      } catch {
        return false;
      }
    })
    .withMessage({
      code: UnprocessableEntity,
      message: "filter.type: parameter has incorrect format",
    })
    .bail()
    .custom((value) => {
      const parsed = JSON.parse(value);
      return parsed.every((type) => TYPES.includes(type));
    })
    .withMessage({
      code: UnprocessableEntity,
      message: "filter.type: parameter has incorrect value",
    }),
  query("sort.name").optional().isIn(SORTING).withMessage({
    code: UnprocessableEntity,
    message: "sort.name: parameter has incorrect value",
  }),
  query("sort.createdAt").optional().isIn(SORTING).withMessage({
    code: UnprocessableEntity,
    message: "sort.createdAt: parameter has incorrect value",
  }),
  validate,
];

const addImage = [
  param("id").isString().withMessage({
    code: UnprocessableEntity,
    message: "id: parameter has incorrect format",
  }),
  body()
    .custom((_, { req }) => req.files?.file[0])
    .withMessage({
      code: UnprocessableEntity,
      message: "file: parameter is required",
    })
    .withMessage({
      code: UnprocessableEntity,
      message: "files.file: only image files are allowed to upload",
    }),
  validate,
];

const removeImage = [
  param("companyId").isString().withMessage({
    code: UnprocessableEntity,
    message: "companyId: parameter has incorrect format",
  }),
  param("imageId").isString().withMessage({
    code: UnprocessableEntity,
    message: "imageId: parameter has incorrect format",
  }),
  validate,
];

module.exports = {
  getOne,
  editOne,
  addImage,
  removeImage,
  createOne,
  deleteOne,
  getMany,
};

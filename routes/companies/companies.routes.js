const multer = require("multer");
const { Router } = require("express");
const actions = require("./companies.actions");
const validator = require("./companies.validator");
const config = require("../../config");

const fileHandler = multer({
  dest: config.images.uploadsDir,
  fileFilter: (_, file, cb) => {
    if (
      file.mimetype === "image/png" ||
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/gif"
    ) {
      cb(null, true);
    } else {
      cb(new Error("invalid_image_type"), false);
    }
  },
});

module.exports = Router()
  .post("/companies", ...validator.createOne, actions.createOne)
  .get("/companies", ...validator.getMany, actions.getMany)
  .get("/companies/:id", ...validator.getOne, actions.getOne)
  .patch("/companies/:id", ...validator.editOne, actions.editOne)
  .delete("/companies/:id", ...validator.deleteOne, actions.deleteOne)
  .post(
    "/companies/:id/image",
    fileHandler.fields([{ name: "file", maxCount: 1 }]),
    ...validator.addImage,
    actions.addImage
  )
  .delete(
    "/companies/:companyId/image/:imageId",
    ...validator.removeImage,
    actions.removeImage
  );

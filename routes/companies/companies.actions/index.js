module.exports = {
  ...require("./get-one.companies.action"),
  ...require("./edit-one.companies.action"),
  ...require("./add-image.companies.action"),
  ...require("./remove-image.companies.action"),
  ...require("./create-one.companies.action"),
  ...require("./delete-one.companies.action"),
  ...require("./get-many.companies.action"),
};

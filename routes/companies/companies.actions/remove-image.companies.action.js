const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { NO_CONTENT } = require("../../../constants/http-codes");
const imageService = require("../../../services/image.service");
const { NotFound } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * DELETE /companies/:id/image
 * Эндпоинт удаления изображения компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function removeImage(req, res) {
  logger.init("remove company image");
  const data = matchedData(req);

  const photo = await db.photo.findUnique({
    where: { id: data.imageId },
    include: { company: true },
  });
  if (!photo) {
    throw new NotFound("Photo not found");
  }

  await Promise.allSettled([
    imageService.removeImage(photo.filepath),
    imageService.removeImage(photo.thumbpath),
  ]);

  res.status(NO_CONTENT).json();
  logger.success();
}

module.exports = {
  removeImage,
};

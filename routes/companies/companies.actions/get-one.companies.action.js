const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const { NotFound } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * GET /companies/:id
 * Эндпоинт получения данных компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function getOne(req, res) {
  logger.init("get company");
  const data = matchedData(req);

  const company = await db.company.findUnique({
    where: { id: data.id },
    include: { contact: true, photos: true },
  });
  if (!company) {
    throw new NotFound("Company not found");
  }

  res.status(OK).json(company);
  logger.success();
}

module.exports = {
  getOne,
};

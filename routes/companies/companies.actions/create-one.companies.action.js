const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { CREATED } = require("../../../constants/http-codes");
const { NotFound } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * POST /companies
 * Эндпоинт создания нового компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function createOne(req, res) {
  logger.init("create company");
  const data = matchedData(req);

  if (data.contactId) {
    const contact = await db.contact.findFirst({
      where: { id: data.contactId },
    });
    if (!contact) {
      throw new NotFound("Contact not found");
    }
  }

  const comapny = await db.company.create({
    data,
    include: { contact: true, photos: true },
  });

  res.status(CREATED).json(comapny);
  logger.success();
}

module.exports = {
  createOne,
};

const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const { db } = require("../../../services/database.service");

/**
 * GET /companies/
 * Эндпоинт получения данных компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function getMany(req, res) {
  logger.init("get companies");
  const data = matchedData(req);

  const take = data.take || 10;

  const payload = {
    take: data.endingBefore ? -Number(take) : Number(take),
    skip: data.startingAfter || data.endingBefore ? 1 : 0,
    include: { contact: true, photos: true },
    orderBy: [],
  };

  if (data.startingAfter || data.endingBefore) {
    payload.cursor = {
      id: data.startingAfter || data.endingBefore,
    };
  }

  if (data.filter?.status) {
    payload.where = {
      ...payload.where,
      status: data.filter.status,
    };
  }

  if (data.filter?.type) {
    const parsed = JSON.parse(data.filter.type);
    payload.where = {
      ...payload.where,
      OR: parsed.map((type) => ({
        type: {
          has: type,
        },
      })),
    };
  }

  if (data.sort?.name) {
    payload.orderBy.push({
      name: data.sort.name,
    });
  }

  if (data.sort?.createdAt) {
    payload.orderBy.push({
      createdAt: data.sort.createdAt,
    });
  }

  const count = await db.company.count({
    where: payload.where,
  });
  const companies = await db.company.findMany(payload);

  if (companies.length === 0) {
    res
      .status(OK)
      .json({ items: [], count, nextCursors: null, previousCursor: null });
    return;
  }

  const nextItem = await db.company.findMany({
    take: 1,
    skip: 1,
    cursor: {
      id: companies[companies.length - 1].id,
    },
    where: payload.where,
  });

  const previousItem = await db.company.findMany({
    take: -1,
    skip: 1,
    cursor: {
      id: companies[0].id,
    },
    where: payload.where,
  });

  res.status(OK).json({
    items: companies,
    count,
    nextCursor: nextItem.length ? companies[companies.length - 1].id : null,
    previousCursor: previousItem.length ? companies[0].id : null,
  });

  logger.success();
}

module.exports = {
  getMany,
};

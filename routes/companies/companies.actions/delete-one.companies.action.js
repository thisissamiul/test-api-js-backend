const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { NO_CONTENT } = require("../../../constants/http-codes");
const { NotFound } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * DELETE /companies/:id
 * Эндпоинт удаления компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function deleteOne(req, res) {
  logger.init("delete company");
  const data = matchedData(req);

  const company = await db.company.findUnique({ where: { id: data.id } });
  if (!company) {
    throw new NotFound("Company not found");
  }

  await db.company.delete({ where: { id: data.id } });

  res.status(NO_CONTENT).send();
  logger.success();
}

module.exports = {
  deleteOne,
};

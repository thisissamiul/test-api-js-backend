const { matchedData } = require("express-validator");
const logger = require("../../../services/logger.service")(module);
const { OK } = require("../../../constants/http-codes");
const { NotFound } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * PATCH /companies/:id
 * Эндпоинт редактирования данных компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function editOne(req, res) {
  logger.init("edit company");
  const data = matchedData(req);

  const company = await db.company.findUnique({ where: { id: data.id } });
  if (!company) {
    throw new NotFound("Company not found");
  }

  if (data.contactId) {
    const contact = await db.contact.findFirst({
      where: { id: data.contactId },
    });
    if (!contact) {
      throw new NotFound("Contact not found");
    }
  }

  const updatedCompany = await db.company.update({
    where: { id: data.id },
    data: { ...data, contactId: data.contactId ?? null },
    include: {
      contact: true,
      photos: true,
    },
  });

  res.status(OK).json(updatedCompany);
  logger.success();
}

module.exports = {
  editOne,
};

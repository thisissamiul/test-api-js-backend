const crypto = require("crypto");
const path = require("path");
const logger = require("../../../services/logger.service")(module);
const { getFileUrl } = require("../../../helpers/url.helper");
const { OK } = require("../../../constants/http-codes");
const imagesConfig = require("../../../config").images;
const imageService = require("../../../services/image.service");
const { NotFound } = require("../../../constants/errors");
const { db } = require("../../../services/database.service");

/**
 * POST /companies/:id/image
 * Эндпоинт добавления изображения компании.
 * @param {Object} req
 * @param {Object} res
 * @return {Promise<void>}
 */
async function addImage(req, res) {
  logger.init("add company image");
  const { id } = req.params;
  const file = req.files.file[0];

  const company = await db.company.findUnique({ where: { id } });
  if (!company) {
    throw new NotFound("Company not found");
  }

  const fileExtension = path.extname(file.originalname).toLowerCase();
  const fileName = crypto.randomBytes(10).toString("hex");

  const uploadedFileName = fileName + fileExtension;
  const uploadedFileThumbName = `${fileName}_${imagesConfig.thumbSize}x${imagesConfig.thumbSize}${fileExtension}`;

  const tempFilePath = file.path;
  const targetFilePath = path.resolve(
    `${imagesConfig.imagesDir}${id}/${uploadedFileName}`
  );
  const targetThumbPath = path.resolve(
    `${imagesConfig.imagesDir}${id}/${uploadedFileThumbName}`
  );

  await imageService.resizeImage(tempFilePath, targetThumbPath);
  await imageService.renameImage(tempFilePath, targetFilePath);

  const uploadedImage = await db.photo.create({
    data: {
      name: uploadedFileName,
      url: getFileUrl(req, uploadedFileName),
      thumbUrl: getFileUrl(req, uploadedFileThumbName),
      filepath: targetFilePath,
      thumbpath: targetThumbPath,
      companyId: id,
    },
    select: {
      id: true,
      name: true,
      url: true,
      thumbUrl: true,
      companyId: true,
    },
  });

  res.status(OK).json(uploadedImage);
  logger.success();
}

module.exports = {
  addImage,
};

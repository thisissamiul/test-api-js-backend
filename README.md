# Тестовое задание для Backend разработчика

## Getting Started

### Installing 
1. Clone the Repository
2. Install Dependencies

```bash
npm install
```

3. Create `.env` file. Use `.env.example` as a template.

#### Database Setup
1. Ensure your database is running and setup the connection in the `.env` file.
2. Run Prisma Migrations

```bash
npx prisma migrate dev
```

3.  Seed the Database

```bash
npx prisma db seed
```

### Running the Application

```bash
npm run start-dev
```

## Constraints
* Basic JWT authentication is used.
* Any user can access any resources.
* Duplicate companies can be created due to uncertainty about the unique identifier of a company.
* A company has only two statuses, allowing filtering by one status at a time.

## Refactorings/Optimizations
* Removed the potentially problematic line `app.set("json replacer", (k, v) => (v === null ? undefined : v))`. This adjustment ensures that null items are not excluded from responses, thereby maintaining consistency for nullable fields in the frontend.
* Implemented stricter file upload validation by verifying mime types using Multer, replacing the previous method that checked only file extensions. This enhances security and reliability in handling uploaded files.
* Addressed a bug in `getFileUrl` where the static path was missing from the URL. This correction ensures accurate file retrieval by including the necessary static path.
* Implemented a database update to store the exact paths of photos and their thumbnails. This ensures future-proofing against potential changes in the photo storage location. By maintaining the full absolute path, critical operations like deletion remain feasible even if the storage location changes.
* Reconfigured photo paths to incorporate a company ID instead of a user ID. This change aligns photo ownership with the corresponding company, simplifying data management and facilitating any future modifications related to photos in the current implementation.
* Improved the application's security by relocating the JWT token from the codebase to environment variables. This move shields sensitive authentication details, aligning with best security practices and simplifying token management.

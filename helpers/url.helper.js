const { matchedData } = require("express-validator");
const config = require("../config");

/**
 * Возвращает URL на основании указанного запроса.
 * @param {Object} req
 * @return {string}
 */
function getUrlForRequest(req) {
  const { port } = config;
  return `${req.protocol}://${req.hostname}${
    port === "80" || port === "443" ? "" : `:${port}`
  }`;
}

/**
 * Возвращает URL файла.
 * @param {Object} req
 * @param {string} fileName
 * @return {string}
 */
function getFileUrl(req, fileName) {
  const data = matchedData(req);
  const { staticPath } = config;
  const url = getUrlForRequest(req);
  return `${url}${staticPath}/images/${data.id}/${fileName}`;
}

module.exports = { getUrlForRequest, getFileUrl };

const config = require("./config.global");

config.jwt.secretKey = process.env.JWT_SECRET;
config.jwt.verify.maxAge = 604800;

module.exports = config;

/* eslint-disable no-await-in-loop */

const { PrismaClient, CompanyType } = require("@prisma/client");
const bcrypt = require("bcrypt");
const { fakerRU: faker } = require("@faker-js/faker");

const prisma = new PrismaClient();

async function seed() {
  // create user
  const hashedPassword = await bcrypt.hash("password", 10);
  await prisma.user.create({
    data: {
      email: "admin@example.com",
      password: hashedPassword,
      fullName: "John Doe",
    },
  });

  const entities = ["ООО", "АО", "ПАО"];

  function getRandomCompanyTypes() {
    const count = Math.random() < 0.5 ? 1 : 2;
    const shuffledTypes = Object.values(CompanyType).sort(
      () => 0.5 - Math.random()
    );
    return shuffledTypes.slice(0, count);
  }

  // create 100 fake contacts & companies using faker
  for (let i = 0; i < 100; i += 1) {
    await prisma.company.create({
      data: {
        name: faker.company.name(),
        shortName: faker.company.name(),
        address: faker.location.streetAddress(),
        businessEntity: entities[Math.floor(Math.random() * entities.length)],
        contractIssueDate: faker.date.past(),
        contractNo: String(faker.number.int()),
        status: "active",
        type: getRandomCompanyTypes(),
        createdAt: faker.date.past(),
        updatedAt: faker.date.recent(),
        contact: {
          create: {
            email: faker.internet.email(),
            firstname: faker.person.firstName(),
            lastname: faker.person.lastName(),
            patronymic: faker.person.middleName(),
            phone: faker.phone.number(),
          },
        },
      },
    });
  }
}

seed()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
